import unittest

class TestGridChallenge(unittest.TestCase):
    def test_gridChallenge(self):
        # Test case 1
        grid = ["ebacd", "fghij", "olmkn", "trpqs", "xywuv"]
        result = gridChallenge(grid)
        self.assertEqual(result, "YES")

        # Test case 2
        grid = ["abc", "lmn", "xyz"]
        result = gridChallenge(grid)
        self.assertEqual(result, "NO")

        # Test case 3
        grid = ["aaa", "bbb", "ccc"]
        result = gridChallenge(grid)
        self.assertEqual(result, "YES")

if __name__ == '__main__':
    unittest.main()
