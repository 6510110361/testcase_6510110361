import unittest

class TestAlternate(unittest.TestCase):
    def test_alternate(self):
        # Test case 1
        s = "beabeefeab"
        expected_output = 5
        self.assertEqual(alternate(s), expected_output)
        
        # Test case 2
        s = "abcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcd"
        expected_output = 0
        self.assertEqual(alternate(s), expected_output)
        
        # Test case 3
        s = "a"
        expected_output = 0
        self.assertEqual(alternate(s), expected_output)
        
        # Test case 4
        s = "aaa"
        expected_output = 0
        self.assertEqual(alternate(s), expected_output)
        
        # Test case 5
        s = "aaab"
        expected_output = 1
        self.assertEqual(alternate(s), expected_output)

if __name__ == '__main__':
    unittest.main()
