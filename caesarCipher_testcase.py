import unittest

class TestCaesarCipher(unittest.TestCase):
    def test_caesar_cipher(self):
        self.assertEqual(caesarCipher('abc', 2), 'cde')
        self.assertEqual(caesarCipher('def', 2), 'fgh')
        self.assertEqual(caesarCipher('XYZ', 2), 'ZAB')
        self.assertEqual(caesarCipher('abc', 28), 'cde')
        self.assertEqual(caesarCipher('abc', 52), 'abc')

if __name__ == '__main__':
    unittest.main()
