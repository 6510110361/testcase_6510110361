#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'gridChallenge' function below.
#
# The function is expected to return a STRING.
# The function accepts STRING_ARRAY grid as parameter.
#

def gridChallenge(grid):
    # Write your code here
    alpha="abcdefghijklmnopqrstuvwxyz"
    grid1=[]
    for m in grid:
        num=[]
        for k in range(len(m)):
            num.append(alpha.index(m[k]))
        num.sort()
        str1=""
        for l in num:
            str1+=alpha[l]
            
        grid1.append(str1)
    #print(grid1)    
    
    count=0
    for j in range(len(grid1[0])):
        for i in range(len(grid1)-1):
        
    
            if alpha.index(grid1[i][j])>alpha.index(grid1[i+1][j]):
                count+=1
            else:
                count+=0
    if count>0:
        return "NO"
    else:
        return "YES"

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    t = int(input().strip())

    for t_itr in range(t):
        n = int(input().strip())

        grid = []

        for _ in range(n):
            grid_item = input()
            grid.append(grid_item)

        result = gridChallenge(grid)

        fptr.write(result + '\n')

    fptr.close()
