#!/bin/python3

import math
import os
import random
import re
import sys
import string

#
# Complete the 'caesarCipher' function below.
#
# The function is expected to return a STRING.
# The function accepts following parameters:
#  1. STRING s
#  2. INTEGER k
#

def caesarCipher(s, k):
    # Write your code here
    k = k % 26
    alpha_l = string.ascii_lowercase
    alpha_u = string.ascii_uppercase
    unenc = {}
    for alpha in (alpha_l, alpha_u):
        shifted_alpha = alpha[k:] + alpha[:k]
        unenc.update(zip(alpha, shifted_alpha))
    unenc_s = (unenc.get(c, c) for c in s)
    unenc_s = "".join(unenc_s)
    return unenc_s

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input().strip())

    s = input()

    k = int(input().strip())

    result = caesarCipher(s, k)

    fptr.write(result + '\n')

    fptr.close()
