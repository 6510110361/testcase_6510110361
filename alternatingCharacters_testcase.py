import unittest

class TestAlternatingCharacters(unittest.TestCase):
    def test_alternating_characters(self):
        self.assertEqual(alternatingCharacters('AAAA'), 3)
        self.assertEqual(alternatingCharacters('BBBBB'), 4)
        self.assertEqual(alternatingCharacters('ABABABAB'), 0)
        self.assertEqual(alternatingCharacters('BABABA'), 0)
        self.assertEqual(alternatingCharacters('AAABBB'), 4)

if __name__ == '__main__':
    unittest.main()
